import React, { useEffect, useState } from 'react';
import { registerFocusManger } from 'renative';
import { withNavigation } from 'react-navigation';
import TopMenu from '../components/topMenu';

const styles = {
    videoPlayer: {
        position: 'fixed',
        alignSelf: 'center',
        width: '100vw',
        height: '100vh',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        border: 'none',
        margin: '10px',
    }
};

let SRGLetterbox = null;
let srgLetterboxPlayer = null;
let isLocal = true;

function improveLayoutIfNeeded() {
    if (srgLetterboxPlayer) {
        srgLetterboxPlayer.player.ready(() => {
            const videoEl = document.querySelector('.vjs-srgssr-skin');
            if (videoEl && videoEl.classList.contains('vjs-fill')) {
                videoEl.classList.remove('vjs-fill');
                videoEl.classList.add('vjs-fluid');
            }
        })
    }
}

function playURN(urn) {
    console.log('playURN');
    srgLetterboxPlayer.setPlayerFocus(false);
    if (srgLetterboxPlayer.getUrn() === urn) {
        srgLetterboxPlayer.paused() ? srgLetterboxPlayer.play() : srgLetterboxPlayer.pause();
    }else {
        srgLetterboxPlayer.prepareToPlayURN(urn, undefined, undefined, 'any');
        // srgLetterboxPlayer.play();
    }
}

/*
* Work around to remove renative script for focused element
* */
function removeFocusedScript() {
    const focusedStyle = document.createElement('style');
    focusedStyle.rel = 'stylesheet';
    focusedStyle.type = 'text/css';
    focusedStyle.appendChild(
        document.createTextNode(`:focus {
          border: none !important;
}
`),
    );
    document.head.appendChild(focusedStyle);
}

const Player = (props) => {
    console.log('Player INIT', props.navigation.isFocused());
    const baseUrl = '//letterbox-web-2.s3.amazonaws.com/stage'; // develop
    // const baseUrl = '//letterbox-web-2.s3.amazonaws.com/test_rinaldo';
    const script = document.createElement('script');
    const [textTracks, setTextTracks] = useState([]);
    const topMenuRef = React.createRef();

    const [state, setState] = useState(false);
    const [topMenu, setTopMenu] = useState(false);
    const [drawer, setDrawer] = useState(false);


    function keyDownHandler(e) {
        if (!props.navigation.isFocused()) {
            return;
        }
        const parent = props.navigation.dangerouslyGetParent().dangerouslyGetParent();
        switch (e.key) {
            case 'ArrowUp':
                console.log('keyDownHandler player textTracks', textTracks);
                if (!parent.state.isDrawerOpen && textTracks.length > 0) {
                    setTopMenu(!topMenu);
                }
                break;
            case 'ArrowLeft':
                if (!topMenu) {
                    props.navigation.toggleDrawer();
                }
                break;
            default:
                break;
        }
    }

    function initializePlayer() {
        const optionsParam = {
            debug: false,
            ilHost: 'il.srgssr.ch',
            fillMode: true,
            playerFocus: false,
            hdMode: undefined,
            language: 'en',
            controls: false,
            controlBar: false,
            headerComponent: false,
            muted: false,
            subdivisionsContainer: false,
        };

        srgLetterboxPlayer = new window.SRGLetterbox(optionsParam);

        srgLetterboxPlayer.playerOptions.controls = false;
        srgLetterboxPlayer.playerOptions.muted = true;
        srgLetterboxPlayer.playerOptions.debug = false;
        srgLetterboxPlayer.playerOptions.playerFocus = false;
        srgLetterboxPlayer.playerOptions.userActions = {};
        srgLetterboxPlayer.playerOptions.headerComponent = false;
        srgLetterboxPlayer.playerOptions.subdivisionsContainer = false;

        srgLetterboxPlayer.initializeOnElement('letterboxWebPlayer');

        srgLetterboxPlayer.player.on('canplay', () => {
            if (window.videojs.getPlayers().vjs_video_3.textTracks()) {
                const subtitles = window.videojs.getPlayers().vjs_video_3.textTracks().tracks_.filter(track => track.kind === 'subtitles' || track.kind === 'captions');
                console.log('subtitles', subtitles);
                setTextTracks(subtitles);
            }
        });

        // playURN('urn:swi:video:44927366'); // swi mp4
        // playURN('urn:rts:video:10719425') // subtitle
        // playURN('urn:rts:video:3608506'); // rts live
        // playURN('urn:srf:video:c4927fcf-e1a0-0001-7edd-1ef01d441651'); // srf live
        // playURN('urn:rts:video:_andreteststream1'); // hls
        // playURN('urn:rts:video:9314220');
        playURN('urn:swi:video:44927366'); // swi mp4

        // improveLayoutIfNeeded();
    }

    useEffect(() => {
        removeFocusedScript();

        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = `${baseUrl}/letterbox.css`;
        link.media = 'all';

        document.head.appendChild(link);

        initializePlayer();

        try {
            // get the urn from the program screen
            props.navigation.addListener('didFocus', (e) => {
                if (e.action.params !== undefined && e.action.params.urn !== undefined && props.navigation.isFocused()){
                    playURN(e.action.params.urn);
                }
            });
        } catch (e) {
            console.log('Player - getting URN form Program screen -  ERROR', e);
        }

        // to have the good opacity in the drawer menu and prevent jump on the thumbbutton selection using keyboard event
        registerFocusManger({ focused: 'opacity: 1' });
    }, []);

    useEffect(() => {
        document.addEventListener('keydown', keyDownHandler);

        return function cleanup() {
            document.removeEventListener('keydown', keyDownHandler);
        };
    });

    useEffect(() => {
        console.log('isFocused', props.navigation.isFocused());
    }, [props.navigation.isFocused()]);


    return (
        <React.Fragment>
            <TopMenu ref={topMenuRef} playerProps={props} textTracks={textTracks} state={topMenu} />
            <div accessible="false" id="letterboxWebPlayer" style={styles.videoPlayer} tabIndex={-1} />
        </React.Fragment>
    );
};

// export default withNavigation(Player);
export default Player;
