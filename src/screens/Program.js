import React, { useEffect, useState, useRef, createRef } from 'react';
import { Text, View, ScrollView } from 'react-native';
import ThumbButton from '../components/thumbButton';
import { useFetchArr } from '../hooks/useFetch';
import {registerFocusManger} from "renative";


const styles = {
    title: {
        fontSize: 20,
        margin: 20
    },
    button: {
        marginTop: 10,
        minWidth: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        margin: 20,
    },
};
const renderElement = <View><Text style={styles.title}> Loading ! </Text></View>;
const mediaListSWIUrl = 'http://il.srgssr.ch/integrationlayer/2.0/swi/mediaList/video/mostClicked';
const mediaListRTSUrl = 'http://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/video/mostClicked';
const mediaListRTSLIVEUrl = 'http://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/video/livestreams';
const mediaListSRFUrl = 'http://il.srgssr.ch/integrationlayer/2.0/srf/mediaList/video/mostClicked';
const mediaListAudioLiveRTSUrl = 'https://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/audio/livestreams';

const Program = (props) => {
    const indexRef = useRef(0);
    const focusable = useRef(true);
    const [accessible, setAccessible] = useState(true);
    const scrollViewRef = createRef();
    let swimLaneRef0 = createRef();
    let swimLaneRef1 = createRef();
    let swimLaneRef2 = createRef();

    const [data, dataLoading] = useFetchArr([mediaListSWIUrl, mediaListRTSUrl, mediaListRTSLIVEUrl, mediaListSRFUrl, mediaListAudioLiveRTSUrl]);
    console.log('test');
    // return <View><Text>Loading...</Text></View>;

    function createThumbButtons(arr) {
        return arr.map((data, index) => (
            <ThumbButton
                key={data.title.charAt(0) + index}
                accessible={accessible}
                style={styles.button}
                title={data.title}
                className="focusable"
                imageUrl={data.imageUrl}
                onPress={() => {
                    console.log('data', data);
                    // props.navigation.navigate('Player', { urn: data.urn });
                }}
                onFocus={() => {
                    indexRef.current = index;
                    console.log('onFocus', indexRef.current);
                }}
            />
        ));
    }

    function closeDrawer() {
        props.navigation.closeDrawer();
        focusable.current = true;
        setAccessible(true);
    }

    function openDrawer() {
        props.navigation.openDrawer();
        focusable.current = false;
        setAccessible(false);
    }

    function keyDownHandler(event) {
        console.log('Program - keyDownHandler');
        // if (event.key === 'ArrowLeft' && indexRef.current === 0) {
        //     try {
        //         console.log('isDrawerOpen', isDrawerOpen);
        //         // eslint-disable-next-line no-unused-expressions
        //         isDrawerOpen === true
        //             ? closeDrawer()
        //             : openDrawer();
        //     } catch (error) {
        //         console.log('openDrawer error', error);
        //     }
        // }
    }

    useEffect(() => {
        console.log('Program useEffect for init');





        // document.addEventListener('keydown', keyDownHandler);
        // props.navigation.addListener('action', (e) => {
        //     if (e.action.type === 'Navigation/DRAWER_CLOSED') {
        //         setAccessible(true);
        //     }
        // });
    }, []);

    useEffect(() => {
        try {
            console.log('Program useEffect for init', window.SpatialNavigation);
        } catch (e) {
            console.log('Program SpatialNavigation ERROR', e);
        }
    }, [window.SpatialNavigation]);

    // useEffect(() => {
    //     console.log('dataLoading', data);
    // }, [dataLoading]);

// return null;
    return (
        <ScrollView ref={scrollViewRef}>
            {dataLoading ? renderElement : (
                <View >
                    <Text style={styles.title}>SWI most clicked</Text>
                    <View style={styles.buttonContainer}  ref={ref => (swimLaneRef0 = ref)}>
                        {createThumbButtons(data[0].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS most clicked</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[1].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS Live</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[2].mediaList)}
                    </View>

                    <Text style={styles.title}>SRF most clicked</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[3].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS audio live</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[4].mediaList)}
                    </View>

                </View>
            )}
        </ScrollView>
    );
};

export default Program;

