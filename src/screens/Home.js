import React, { useEffect, useState, useRef, createRef } from 'react';
import { Text, Image, View, TouchableOpacity, StyleSheet, ScrollView, PixelRatio } from 'react-native';
import { Icon, Button, Api, getScaledValue, isWatch } from 'renative';
import Theme from '../old/theme';

const styles = StyleSheet.create({
    appContainerScroll: {
        flex: 1,
        paddingTop: getScaledValue(50)
    },
    appContainerView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: getScaledValue(30),
    },
    textH2: {
        fontFamily: Theme.primaryFontFamily,
        fontSize: getScaledValue(20),
        marginHorizontal: getScaledValue(20),
        color: Theme.color4,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    textH3: {
        fontFamily: Theme.primaryFontFamily,
        fontSize: getScaledValue(15),
        marginHorizontal: getScaledValue(20),
        marginTop: getScaledValue(5),
        color: Theme.color2,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    image: {
        marginBottom: getScaledValue(30),
        width: getScaledValue(83),
        height: getScaledValue(97),
    },
    buttonWear: {
        minWidth: getScaledValue(130)
    },
    button: {
        minWidth: getScaledValue(150)
    }
});

const isWear = isWatch();
const selectedStyle = isWear ? styles.appContainerView : styles.appContainerScroll;
const styleButton = isWear ? styles.buttonWear : styles.button;
const SelectedView = isWear ? View : ScrollView;
let state = { bgColor: Theme.color1 };

const Home = (props) => {
    useEffect(() => {

    }, []);


        return (
            <SelectedView
                style={[selectedStyle, { backgroundColor: state.bgColor }]}
                contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Button
                    style={styleButton}
                    title="Now Try Me!"
                    className="focusable"
                    onPress={() => {
                        Api.navigation.navigate('Program');
                    }}
                />
            </SelectedView>
        );
};

export default Home;
