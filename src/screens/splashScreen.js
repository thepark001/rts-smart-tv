import React from 'react';
import { View, Text } from 'react-native';


const styles = {
    viewStyles: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red'
    },
    textStyles: {
        width: '100%',
        height: '100%',
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold'
    }
}

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log('SplashScreen constructor');
    }

    componentDidMount() {
        console.log('SplashScreen componentWillUnmount');
    }

    componentWillReceiveProps = (nextProps) => {
        console.log('SplashScreen componentWillReceiveProps', nextProps);
    };

    componentWillUnmount() {
        console.log('SplashScreen componentWillUnmount');
    }


    render() {
        return (
            <View style={styles.viewStyles}>
                <Text style={styles.textStyles}>
                    Splash Screen
                </Text>
            </View>
        );
    }
}


export default SplashScreen;
