import React, { Component } from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import KeyEvent from 'react-native-keyevent';
import { WebView } from 'react-native-webview';

const styles = StyleSheet.create({
    view: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        marginHorizontal: 20,
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
});

class Player extends Component {
    constructor(props) {
        super(props);
        console.log('Player constructor');
    }

    componentDidMount() {
        console.log('Player componentDidMount', KeyEvent);
        // if you want to react to keyDown
        KeyEvent.onKeyDownListener((keyEvent) => {
            console.log(`onKeyDown keyCode: ${keyEvent.keyCode}`);
            console.log(`Action: ${keyEvent.action}`);
            console.log(`Key: ${keyEvent.pressedKey}`);
        });

        // if you want to react to keyUp
        KeyEvent.onKeyUpListener((keyEvent) => {
            console.log(`onKeyUp keyCode: ${keyEvent.keyCode}`);
            console.log(`Action: ${keyEvent.action}`);
            console.log(`Key: ${keyEvent.pressedKey}`);
        });

        // if you want to react to keyMultiple
        KeyEvent.onKeyMultipleListener((keyEvent) => {
            console.log(`onKeyMultiple keyCode: ${keyEvent.keyCode}`);
            console.log(`Action: ${keyEvent.action}`);
            console.log(`Characters: ${keyEvent.characters}`);
        });
    }

    render() {
        return (
            <View style={styles.view}>
                <WebView
                    source={{uri: 'file:///android_asset/index-player.html'}}
                    originWhitelist={['*', 'https://*']}
                    allowFileAccess
                    allowUniversalAccessFromFileURLs
                />
            </View>
        );
    }
}

export default Player;
