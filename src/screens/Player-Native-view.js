import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
    view: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        marginHorizontal: 20,
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
});

class Player extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.webView = null;
        console.log('Player constructor');
        this.sendPostMessage.bind(this);
        this.playURN.bind(this);

        // KeyEvent.onKeyDownListener(keyEvent => {
        //   console.log(`onKeyDown keyCode: ${keyEvent.keyCode}`);
        //
        //   switch (keyEvent.keyCode) {
        //     case 19:
        //       console.log('keyEvent 19 - openDrawer');
        //       this.props.navigation.openDrawer();
        //       break;
        //   }
        // });

        // try {
        //     props.navigation.addListener('didFocus', (e) => {
        //         console.log('didFocus', e);
        //         if (
        //             e.action.params !== undefined
        //             && e.action.params.urn !== undefined
        //             && props.navigation.isFocused()
        //         ) {
        //             this.playURN(e.action.params.urn);
        //         }
        //     });
        //
        //     props.navigation.addListener('willBlur', () => {
        //         console.log('Player willBlur - ', props.navigation.isFocused());
        //     });
        // } catch (e) {
        //     console.log('Player - didFocus willBlur ERROR', e);
        // }
    }

    componentDidMount() {
        console.log('Player componentDidMount');
    }

    onMessage() {
        console.log('onMessage');
    }

    sendPostMessage(data) {
        console.log('Sending post message', data);
        this.webView.postMessage(JSON.stringify({data}), '*');
    }

    playURN(urn) {
        console.log('playURN');
        this.sendPostMessage({ urn });
    }

    render() {
        return (
            <View style={styles.view}>
                <Text>hello world</Text>
            </View>
        );
    }
}

export default Player;
