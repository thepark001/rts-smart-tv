import React from 'react';
import {createApp, registerFocusManger, registerServiceWorker} from 'renative';
import { navStructure } from './navigation/navigation';
import Program from './screens/Program';
import DrawerMenu from './components/drawerMenu';
import Player from './screens/Player-Native-view';
import Home from './screens/Home';

let AppContainer;
registerFocusManger({ focused: 'opacity: 0.1' });
registerServiceWorker();
console.disableYellowBox = true;


function App() {
    AppContainer = createApp(navStructure, { Player, Program, DrawerMenu, Home });
    return AppContainer;
}

export default App;
