import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import Api from 'renative/src/Api';

const hasFocus = Api.formFactor === 'tv' && Api.platform !== 'tvos';
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 10,
    },
    blurState: {
        opacity: 0.7,
    },
    focusState: {
        opacity: 1,
    },
    thumb: {
        height: 84,
        minWidth: 150,
    }
});

const ThumbButton = (props) => {
    const [state, setState] = useState({ currentStyle: styles.blurState });
    const { testID, accessible, className } = props;
    const imageSource = props.imageUrl + '/scale/width/448';

    useEffect(() => {
        console.log('ThumbButton init');
    }, []);

    useEffect(() => {
        // console.log('ThumbButton focusable', accessible);
    }, [accessible]);

    return (
        <TouchableOpacity
            testID={testID}
            accessible={accessible}
            className={className}
            style={[styles.button, state.currentStyle]}
            onPress={() => {
                props.onPress();
            }}
            onFocus={() => {
                setState({ currentStyle: styles.focusState });
                props.onFocus();
            }}
            onBlur={() => {
                setState({ currentStyle: styles.blurState });
            }}
        >

            <Image
                style={styles.thumb}
                source={{uri:imageSource }}
            />
        </TouchableOpacity>
    );
};

export default ThumbButton;
