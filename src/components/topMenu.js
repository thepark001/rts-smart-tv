import React, { useEffect, useState } from 'react';
import {
    Dimensions,
    StyleSheet,
    View,
    Text,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Button from './button';

const styles = StyleSheet.create({
    menu: {
        position: 'absolute',
        width: '100vw',
        flex: 1,
        zIndex: 2,
        backgroundColor: 'rgba(0, 0, 0, .9)',
        padding: 10,
    },
    title: {
        fontSize: 18,
        color: 'white',
    },
    button: {
        margin: 10,
        width: 100,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
});


function TopMenu(props) {
    const [accessible, setAccessible] = useState(true);
    const [currentTextTrack, setCurrentTextTrack] = useState();
    const [visibilityValue, setVisibilityValue] = useState('none');

    function showSubtitle(item) {
        props.textTracks.forEach((textTrack) => {
            textTrack.mode = 'hidden';
        });

        if (item && item !== currentTextTrack) {
            item.mode = 'showing';
            setCurrentTextTrack(item);
        }
    }

    useEffect(() => {
        console.log('state', props.state);
        setVisibilityValue(props.state ? 'flex' : 'none');
        setAccessible(props.state);
    }, [props.state]);


    return (

        <View style={[styles.menu, { display: visibilityValue }]}>
            <Text style={styles.title}>
Sous titre
            </Text>
            <View style={styles.buttonContainer}>

                {props.textTracks.map(texttrack => (
                    <Button
                        onPress={() => {
                            showSubtitle(texttrack);
                        }
                        }
                        accessible={accessible}
                        style={styles.button}
                        key={texttrack.language}
                        title={texttrack.language}
                    />
                ))}
            </View>
        </View>
    );
}

export default withNavigation(TopMenu);
