let _programData; // eslint-disable-line

function setProgramData(data) {
    console.log('setProgramData'. data);
    _programData = data;
}

function getProgramData() {
    return _programData;
}

export default {
    setProgramData,
    getProgramData,
};
