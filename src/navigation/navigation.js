import React from 'react';
import {
    WEB,
    WEBOS,
    TIZEN,
    getScaledValue,
} from 'renative';

const navStructure = {
    root: {
        menus: {
            drawerMenu: {
                isVisibleIn: [WEB, WEBOS, TIZEN],
                component: 'DrawerMenu',
                options: {
                    drawerBackgroundColor: 'rgba(0,0,0,.9)',
                    drawerColorText: 'white',
                    drawerPosition: 'left',
                    drawerType: 'front',
                    initialRouteName: 'Program',
                    contentOptions: {
                        activeTintColor: 'white',
                        inactiveTintColor: 'gray',
                    },
                },
                navigationOptions: {},
            },
        },
        screens: {
            Player: {
                screen: 'Player',
                navigationOptions: {
                    header: null
                },
                stacks: ['Player'],
            },
            Program: {
                screen: 'Program',
                navigationOptions: {
                    header: null
                },
                stacks: ['Program'],
            },
            Home: {
                screen: 'Home',
                navigationOptions: {
                    header: null
                },
                stacks: ['Home'],
            },
        },
    },
    stacks: {
        screens: {
            Player: {
                screen: 'Player',
            },
            Program: {
                screen: 'Program',
            },
            Home: {
                screen: 'Home',
            },
        },
        navigationOptions: {
        },
    },
};

export { navStructure };
