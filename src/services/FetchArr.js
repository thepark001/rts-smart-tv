function FetchArr(arr) {
    let loading = true;
    async function fetchUrl(url) {
        const response = await fetch(url);
        console.log("fetchUrl", response);
        return await response.json();
    }

    const getData = async () => {
        return Promise.all(arr.map(url => fetchUrl(url)));
    };

    getData().then(data => {
        console.log("setData", data);
        loading = false;
        return [data, loading];
    });
}

export { FetchArr };
