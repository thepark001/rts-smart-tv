import React from 'react';
import { createApp, registerFocusManger, registerServiceWorker } from 'renative';
import { navStructure } from './old/nav';
import ScreenHome from './old/screenHome';
import ScreenMyPage from './old/screenMyPage';
import ScreenModal from './old/screenModal';
import Menu from './old/menu';

import '../platformAssets/runtime/fontManager';

registerFocusManger({ focused: 'opacity: 0.4' });registerServiceWorker();


// Flag to enable yellow warning
console.disableYellowBox = true;


let AppContainer;

class App extends React.Component {
    constructor(props) {
        super(props);
        AppContainer = createApp(navStructure, { ScreenHome, ScreenMyPage, ScreenModal, Menu });
    }

    render() {
        return AppContainer;
    }
}

export default App;
